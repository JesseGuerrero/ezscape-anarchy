# **World 4 Features**

## 🌍 **Anarchy World**
- **PvP Enabled World** - Anarchy, every man for himself.
- **No Monitoring for bots** - Anarchy means do what you want.
- **No Iron Man Mode** – Economy driven, barter your spoils.
- **Separate Database** – Secure and **backed up**.

## ⚡ **Game Boosts**
- **100x XP Rate** – Level up fast!

### 🕍 **Church Services**
- Attend services at **Lumbridge Church**:
  - Gain a **+0.001% bonus to all stats** for each service you attend, active **at all times**.

### ⚔️ **Scaled Bosses**
- Scale up boss difficulty by talking to the monk outside Lumbridge Church.
- Monsters with combat level **274+** drop more:  
  **Scale # * (1 + 0.05) * amount dropped!**

### 🔨 **Item Upgrades**
- **Upgrade items by %** each time you use a hammer on them!  

**Upgradeable Items**:
  - **Top Tier**:  
    Dragon Claws, Visage, Dragon Platebody, Fire Cape, Kiln Cape, Fury, Royal Crossbow, Torva Helm, Torva Platebody, Torva Legs, Torva Gloves, Torva Boots, Pernix Cowl, Pernix Body, Pernix Chaps, Pernix Gloves, Pernix Boots, Virtus Mask, Virtus Top, Virtus Legs, Virtus Gloves, Virtus Boots, Zaryte Bow, Bandos Hilt, Armadyl Hilt, Saradomin Hilt, Zamorak Hilt, Arcane Spirit Shield, Divine Spirit Shield, Elysian Spirit Shield, Spectral Spirit Shield, Regen Bracelet, Dragon 2H, Dragon Full Helm.

  - **More Upgradeable Gear**:  
    Dragon Spear, Steadfast, Glaiven, Ragefire, Dragon Chain, D Hatchet, Archers Ring, Berserker Ring, Warrior Ring, Seers Ring, Bandos Helm, Bandos Chestplate, Bandos Tassets, Bandos Boots, Bandos Gloves, Bandos Warshield, Zamorakian Spear, Zamorak Hood, Zamorak Garb, Zamorak Gown, Zamorak Ward, Zamorak Boots, Zamorak Gloves, Saradomin Sword, Saradomin Whisper, Saradomin Hiss, Saradomin Murmur, Armadyl Crossbow, Armadyl Helm, Armadyl Chestplate, Armadyl Legs, Armadyl Boots, Armadyl Buckler, Armadyl Gloves.
  - **Normal tier**: 
    +0.5%<->2%
    
## 🌟 **EZScape Features**
- **Triple Drops** on all items.
- **Double Ore, Fish, and Logs** on every harvest.
- **Minigame Points & Loyalty Boosts**.
  - **Pest Control**: Max at 99.
  - **Soul Wars Zeal**: Max at 99.
- **Infinite Aura Slot** for unlimited power.
- **10x Pickpocket Success Rate**.
- **Save Fight Caves & Fight Kiln** progress.
- **10x Loot from Chests**.
- **Double Farming Harvests** and **Ticks**.
- **1/17 Defender Drop Rate** – Get it fast!
- **5x Slayer Points**.
- **2B XP Cap** – Reach new heights!
- **1x XP after Level 99**.
  
### 🏰 **Dungeoneering Enhancements**
- Solo Larges are now possible!
- **Boss Difficulty Multiplier: 0.8** – Easier dungeons.
- **Dungeoneering Token Multiplier: 50x** – Earn faster.

### 🔥 **Quality of Life Enhancements**
- **Open Clue Rewards** without the puzzles.
- **Slayer Task Amount Reduced** to 1/3.
- **Triple Barrows Rewards**.
- **3x Rune Multiplication** when crafting.
- **Save GWD Killcount**.
- **Blood Talisman added** to Soul Wars gamble.
