package com.rs.rsps;

import com.rs.game.content.bosses.qbd.npcs.*;
import com.rs.game.model.entity.npc.combat.NPCCombatDefinitions;
import com.rs.plugin.annotations.PluginEventHandler;
import com.rs.plugin.annotations.ServerStartupEvent;

import java.util.Map;

@PluginEventHandler
public class SingleplayerBosses {
    @ServerStartupEvent
    public static void checkNerfedLevels() {
        Map<NPCCombatDefinitions.Skill, Integer>
                levels = NPCCombatDefinitions.getDefs(8349).getLevels();
        for(int level : levels.values())
            if(level != 300)
                throw new RuntimeException("TD level is not 300");
    }

    public static NPCCombatDefinitions weakenNPCById(int id, NPCCombatDefinitions defs) {//combatLevel property must be public
        int[] npcIdsToWeaken = {15507, 15506, 15464};
        for(int npcId : npcIdsToWeaken)
            if(id == npcId)
                for(NPCCombatDefinitions.Skill skill : defs.combatLevels.keySet())
                    defs.combatLevels.put(skill, (int) (defs.combatLevels.get(skill) * 0.1));
        return defs;
    }

    public static NPCCombatDefinitions weakenNPCByName(String name, NPCCombatDefinitions defs) {
        String[] npcNamesToWeaken = {"General Graardor"};
        for(String npcName : npcNamesToWeaken)
            if(name.equalsIgnoreCase(npcName))
                for(NPCCombatDefinitions.Skill skill : defs.combatLevels.keySet())
                    defs.combatLevels.put(skill, (int) (defs.combatLevels.get(skill) * 0.8));

        npcNamesToWeaken = new String[]{"Kree'arra", "Corporeal Beast"};
        for(String npcName : npcNamesToWeaken)
            if(name.equalsIgnoreCase(npcName))
                for(NPCCombatDefinitions.Skill skill : defs.combatLevels.keySet())
                    defs.combatLevels.put(skill, (int) (defs.combatLevels.get(skill) * 0.5));
        npcNamesToWeaken = new String[]{"Queen Black Dragon", "Sergeant Steelwill", "Nex", "TzTok-Jad", "TokHaar-Jad", "Har-Aken",
                "Har-Aken (Ranged Tentacle)", "Har-Aken (Magic Tentacle)", "Dagannoth Rex", "Dagannoth Prime", "Dagannoth Supreme", "King Black Dragon"};
        for(String npcName : npcNamesToWeaken)
            if(name.equalsIgnoreCase(npcName))
                for(NPCCombatDefinitions.Skill skill : defs.combatLevels.keySet())
                    defs.combatLevels.put(skill, (int) (defs.combatLevels.get(skill) * 0.1));
        return defs;
    }

    public static int weakenQBD() {
        return 7500;
    }

    public static int delayRepeatWormsQBD() {
        return 32;
    }

    public static int slowQBD() {
        return 2;
    }

    public static QueenAttack[] getPhase(int phase) {
        if(phase == 2)
            return new QueenAttack[]{new FireBreathAttack(), new MeleeAttack(), new RangeAttack(), new MeleeAttack(), new RangeAttack(), new FireWallAttack(), new ChangeArmour(), new SoulSummonAttack()};
        if(phase == 3)
            return new QueenAttack[]{new FireBreathAttack(), new MeleeAttack(), new RangeAttack(),  new MeleeAttack(), new RangeAttack(),  new MeleeAttack(), new RangeAttack(), new FireWallAttack(), new ChangeArmour(), new SoulSummonAttack(), new SoulSiphonAttack()};
        if(phase == 4)
            return new QueenAttack[]{
                    new FireBreathAttack(),
                    new TimeStopAttack(),
                    new MeleeAttack(),
                    new RangeAttack(),
                    new MeleeAttack(),
                    new RangeAttack(),
                    new MeleeAttack(),
                    new RangeAttack(),
                    new MeleeAttack(),
                    new RangeAttack(),
                    new MeleeAttack(),
                    new RangeAttack(),
                    new MeleeAttack(),
                    new RangeAttack(),
                    new FireWallAttack(),
                    new SuperFireAttack(),
                    new ChangeArmour(),
                    new SoulSummonAttack(),
                    new SoulSiphonAttack()
            };
        return new QueenAttack[]{};
    }
}
